const _baseURI = Symbol();
const _gitVersion = Symbol();

export class Users {

	constructor() {
		this[_gitVersion] = "v4";
		this[_baseURI] = "https://gitlab.com/api/" + this[_gitVersion] + "/users/";
	}

	fetchUsers(username, token) {
		
		let method = "GET";
		if (token == null || token == "") {
			// we override the default token
			token = "aDfHujqKwsLxXkFqSjh-";
			console.log("token overridden " + token);
		}
		let searchQuery = "";
		if (username != "") {
			searchQuery = "?search=" + username;
		}
		let currentUri = this[_baseURI] + searchQuery;
		
		console.log("fetch users " + currentUri);
		return new Promise(function (resolve, reject) {
			var xhr = new XMLHttpRequest();
			xhr.open(method, currentUri);
			if (token != null && token != "") {
				xhr.setRequestHeader("PRIVATE-TOKEN", token);
			}
			xhr.wi
			xhr.onload = function () {
				if (this.status >= 200 && this.status < 300) {
					resolve(xhr.response);
				} else {
					reject({
						status: this.status,
						statusText: xhr.statusText
					});
				}
			};
			xhr.onerror = function () {
				reject({
					status: this.status,
					statusText: xhr.statusText
				});
			};
			
			xhr.send();
		});
	}
}