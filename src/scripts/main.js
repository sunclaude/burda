import { Animations } from './utils/animations';
import { UserController } from './controllers/users';

const searchBtn = Symbol();

export class WebMain {

	static main() {
		
		console.log("main has been called");
		var animBuilder = new Animations();
		setTimeout(() => { animBuilder.SearchBoxOpen() }, 500);
		
		this['searchBtn'] = document.getElementById("buttonSearch");
		this['searchBtn'].addEventListener("click", (e) => { WebMain.onSearchButtonClick(e); }, false); 
	}

	static onSearchButtonClick(event) {
		let token = document.getElementById("gittoken").value;
		let username = document.getElementById("gitusername").value;
		UserController.findUserByName(username, token);
		event.preventDefault();
		event.stopPropagation();
	}
	
}