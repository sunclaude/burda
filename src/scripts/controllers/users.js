import { Users } from '../rest/users';

const restUsers = Symbol();

export class UserController {

	constructor() {}
	
	static findUserByName(username, token) {
		this['restUsers'] = new Users();
		this['restUsers'].fetchUsers(username, token)
			.then(function (data) {
				var users = JSON.parse(data);
				
				UserController.updateUIWithUsers(users);
			})
			.catch(function (err) {
				console.error('There was a network error!', err.status);
			});
	}
	
	static updateUIWithUsers(users) {
		
		let rows = "";
		let ths = "<th>Id</th><th>Name</th><th>Username</th>";
		users.forEach((user) => {
			var row = "<tr><td>" + user.id + "</td><td>" + user.name + "</td><td>" + user.username + "</td></tr>";
			rows += row;
		});

		let table = '<table class="result-table">' + ths + rows + '</table>';
		let resultContainer = document.getElementById("result-box");
		resultContainer.innerHTML = table;
		resultContainer.style.opacity = 1;
	}
}