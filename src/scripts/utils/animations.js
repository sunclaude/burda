const _searchContainer = Symbol();
const _loginForm = Symbol();
const _transitionEvent = Symbol();

export class Animations {

	constructor() {
		this[_searchContainer] = document.getElementById("login-container");
		this[_loginForm] = document.getElementById("loginForm");
		Animations.ToggleHideElement(this[_loginForm]);
		
		/* Listen for a transition! */
		this[_transitionEvent] = this._whichTransitionEvent();
		this[_transitionEvent] && window.addEventListener(
			this[_transitionEvent], (event) => { this._transitionManager(this[_loginForm], event) });
	
	}
	
	SearchBoxOpen() {
		console.log("search box open");
		this._setHeight(this[_searchContainer], 400);
	}
	
	_transitionManager(element, event) {
		console.log(event);
		console.log("finished transition");
		Animations.ToggleHideElement(this[_loginForm]);
	}
	
	static ToggleHideElement(element) {
		if (element != null) {
			// toggle
			let style = element.style;
			
			if (style.display === "none") {
				style.display = "block"
			} else {
				style.display = "none";
			}
		}
	}
	
	_setHeight(element, height) {
		if (element != null) {
			element.style.height = height + "px";
		}
	}
	
	_whichTransitionEvent(){
		var t;
		var el = document.createElement("fakeelement");
		var transitions = {
		  'transition':'transitionend',
		  'OTransition':'oTransitionEnd',
		  'MozTransition':'transitionend',
		  'WebkitTransition':'webkitTransitionEnd'
		}

		for(t in transitions){
			if( el.style[t] !== undefined ){
				return transitions[t];
			}
		}
	}



}